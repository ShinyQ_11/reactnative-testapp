import React, {Component} from 'react'
import { StyleSheet, Text, TextInput, Button, View } from 'react-native';

class PlaceInput extends Component{
    state = {
        placeName: ""
    }

    placeNameChangedHandler = val => {
        this.setState({
            placeName: val
        })
    }

    placeSubmitHandler = () => {
        if (this.state.placeName.trim() === "") {
            return;
        }
        
        this.props.onPlaceAdded(this.state.placeName)
    }

    render(){
        return(
            <View style = { styles.inputContainer } >
                
                <TextInput
                    style={styles.inputPlaces}
                    placeholder="Tambahkan Kota ..."
                    value={this.state.placeName}
                    onChangeText={this.placeNameChangedHandler}
                />

                <Button
                    title="Tambah"
                    onPress={ this.placeSubmitHandler }
                />
            </View >
        )
    }
}

const styles = StyleSheet.create({
    inputContainer: {
        marginTop: 20,
        // flex: 1,
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },

    inputPlaces: {
        width: "70%",
    },
})

export default PlaceInput