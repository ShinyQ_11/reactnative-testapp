import React from 'react'
import { StyleSheet, FlatList, ScrollView} from 'react-native';
import ListItem from './ListItem/ListItem'

const PlaceOutput = props => {

    return(
        <FlatList 
            data={props.places} 
            style={styles.placeOutput}
            renderItem={(info) => (
                <ListItem
                    placeName={info.item.name }
                    placeImage={info.item.image}
                    onItemPressed={() => props.onItemSelected(info.item.key)}
                />
            )}
        /> 
    );
}

const styles = StyleSheet.create({
    placeOutput: {
        width: "100%",
        margin: "5%"
    }

});

export default PlaceOutput