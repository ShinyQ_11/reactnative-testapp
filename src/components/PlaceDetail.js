import React from 'react'
import { Modal, View, Image, Text, Button, StyleSheet } from 'react-native'

const placeDetail = props => {
    let modalContent = null

    if(props.selectedPlace){
        modalContent = (
            <View>
                <Image style={styles.placeImage} source={props.selectedPlace ? props.selectedPlace.image : null} />
                <Text style={styles.placeName}>{props.selectedPlace ? props.selectedPlace.name : null}</Text>
            </View>
        )
    }

    return (
        <Modal visible={props.selectedPlace !== null} animationType={"slide"} >
            <View style={styles.modalContainer}>
                {modalContent}
                   
                <View style={styles.buttonContainer}>
                    <View style={styles.detailButton}>
                        <Button title="Delete" color="red" onPress={props.onItemDelete} />
                    </View>
                    <View style={styles.detailButton}>
                        <Button title="Tutup" onPress={props.onModalClose} />
                    </View>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalContainer: {
        margin: 20,
    },
    buttonContainer:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "space-between",
    },
    placeImage:{
        width: "100%",
        height: 200
    },
    placeName: {
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 28,
        marginTop: 16,
        marginBottom: 16
    },
    detailButton:{
        flex: 1
    },
})

export default placeDetail