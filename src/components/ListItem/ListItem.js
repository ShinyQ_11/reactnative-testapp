import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image  } from 'react-native';

const listItem = (props) => (
    <TouchableOpacity onPress={props.onItemPressed}>
        <View style={styles.listitem}>
            <Image resizeMode={"contain"} source={props.placeImage} style={styles.placeImage} />
            <Text>
                {props.placeName}
            </Text>
        </View>
    </TouchableOpacity>
)


const styles = StyleSheet.create({
    listitem:{
        width: '100%',
        padding: 10,
        marginTop: 10,
        backgroundColor: "#eee",
        flexDirection: "row",
        alignItems: "center"
    },
    placeImage:{
        marginRight: 8,
        maxWidth: 150,
        maxHeight: 100,
        width: 150,
        height: 100
    }
})

export default listItem