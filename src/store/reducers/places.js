import { ADD_PLACE, DELETE_PLACE, SELECT_PLACE, UNSELECT_PLACE } from "../actions/actionTypes"

const initialState = {
    places: [],
    selectedPlace: null
}

export default reducer