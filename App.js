import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import PlaceInput from './src/components/PlaceInput'
import PlaceOutput from './src/components/PlaceOutput'
import placeImage from './src/assets/city.jpg'
import ModalDetail from './src/components/PlaceDetail'

export default class App extends Component {
  
  state= {
    places: [],
    selectedPlace: null
  }

  placeNameChangedHandler = val =>{
    this.setState({
      placeName: val
    })
  }

  placeAddedHandler = placeName => {
    this.setState(prevState => {
      return{
        places: prevState.places.concat({
          key: Math.random(), 
          name: placeName,
          // image: placeImage
          image: {
            uri: "https://upload.wikimedia.org/wikipedia/commons/d/dc/Skyscrapers_of_Shinjuku_2009_January_%28revised%29.jpg"
          }
        })
      }
    })
  }

  placeSelectedHandler = (key) => {
    this.setState(prevState => {
        return {
          selectedPlace: prevState.places.find(place => {
            return place.key === key
          })
        }
      })
  }

  placeDeleted = () => {
    this.setState(prevState => {
      return {
        places: prevState.places.filter(place =>{
          return place.key !== prevState.selectedPlace.key
        }),
        selectedPlace: null
      }
    })
  }

  placeModalClosed = () => {
    this.setState({
      selectedPlace: null
    })
  }

  render(){
    return (
      <View style={styles.container}>
        <ModalDetail
          selectedPlace={this.state.selectedPlace} 
          onItemDelete={ this.placeDeleted }
          onModalClose={ this.placeModalClosed }
        />
        <Text style={styles.textTitle}>Tambahkan Kota Favorit</Text>
        <PlaceInput 
          onPlaceAdded={ this.placeAddedHandler } 
        />
        <PlaceOutput 
          places={this.state.places} 
          onItemSelected={ this.placeSelectedHandler }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },

  textTitle:{
    marginTop: 20,
    fontSize: 20,
    fontWeight: "600",
    color: "#38a3f5"
  }
});
